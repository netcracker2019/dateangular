import { Component, OnInit } from '@angular/core';
import {Person, RegistrationService, Roles} from './registration.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.css']
})
export class RegistrationPageComponent implements OnInit {

  name: string; //''
  passwords: string;
  phone: string;
  email: string;
  role: Roles;
  userPasswordProve: '';
  registrationError = 'error here';
  lastResponse: string;

  emailLink$: string;

  //03.05.2019
  hide = true;
  emailControl = new FormControl('', [Validators.required, Validators.email]);
  progress = false;

  constructor(private registrationService: RegistrationService,
              private routes: Router,
              private actRoute: ActivatedRoute) { }

  ngOnInit() {
    this.actRoute.queryParams
      .subscribe(params =>{
        console.log(params['email']);
        if (params['email'] == undefined || params['email'] == null){
          this.emailLink$ = " ";
          console.log(this.emailLink$);
        }
        else {
          this.emailLink$ = params['email'];
          console.log(this.emailLink$);

          this.email = this.emailLink$;
        }
      });
  }
  register() {
    if (this.name === '' || this.name == null) {
      this.registrationError = 'Please, add name';
      alert('Please, add name');
      return;
    }
    if (this.passwords === '' || this.passwords == null) {
      this.registrationError = 'Please, add password';
      alert('Please, add password');
      return;
    }
    if (this.email === '' || this.email == null) {
      this.registrationError = 'Please, add email';
      alert('Please, add email');
      return;
    }
    if (this.passwords !== this.userPasswordProve) {
      this.registrationError = 'Passwords are different';
      alert('Passwords are different');
      return;
    }
    if(this.getErrorMessage() == 'Not a valid email'){
      alert(this.getErrorMessage());
      return;
    }

    //03.05.2019
    this.progress = !this.progress;
    //03.05.2019

    this.registrationService.registerUser(
      new Person(this.name, this.passwords, this.email, this.phone),
      this.emailLink$) // Extra parameter
      .subscribe( response => {
        this.lastResponse = response.body.toString();
        if (this.lastResponse === 'CONFLICT') {
          this.registrationError = 'This login is already taken';
          return;
        }
        if (this.lastResponse === 'OK') {
          this.registrationError = 'Registration has been completed successfully';
          this.routes.navigate(['/auth']);
        }

  });
  }

  goToMain(){
    this.routes.navigate(['/']);
  }

  goToAuth(){
    this.routes.navigate(['/auth']);
  }

  //03.05.2019
  getErrorMessage(){
    return this.emailControl.hasError('required') ? 'You must enter a value' :
      this.emailControl.hasError('email') ? 'Not a valid email' : '' ;
  }

}


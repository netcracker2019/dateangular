import {Observable} from 'rxjs';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';


export enum Roles {
  Admin, User
}

export class Person {
  name: string;
  passwords: string;
  phone: string;
  email: string;
  role: Roles;

  constructor(name: string, password?: string, email?: string, phone?: string, role?: Roles) {
    this.name = name;
    this.passwords = password;
    this.email = email;
    this.phone = phone;
    this.role = role;
  }
}

@Injectable()
export class RegistrationService {

  constructor(private http: HttpClient) {
  }

  registerUser(person: Person, email: string) {
    return this.http.post('http://localhost:8080/registerPerson?email=' + email, person, {observe: 'response'});
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainWwwComponent } from './main-www.component';

describe('MainWwwComponent', () => {
  let component: MainWwwComponent;
  let fixture: ComponentFixture<MainWwwComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainWwwComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainWwwComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

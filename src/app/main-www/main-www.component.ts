import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from "../user.service";
import {faUsers} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-main-www',
  templateUrl: './main-www.component.html',
  styleUrls: ['./main-www.component.css']
})
export class MainWwwComponent implements OnInit {

  currentUser: any;
  faUsers = faUsers;





  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    if (this.userService.currentUser() != null){
      this.router.navigate(['/meetings']);
    }
    console.log(this.userService.currentUser());
  }

  goToAuth(){
    this.router.navigate(['/auth']);
  }

  goToRegistration(){
    this.router.navigate(['/registration']);
  }

}

import { Component, OnInit } from '@angular/core';
import {faFire, faHollyBerry, faUsers, faWalking} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-new-toolbar',
  templateUrl: './new-toolbar.component.html',
  styleUrls: ['./new-toolbar.component.css']
})
export class NewToolbarComponent implements OnInit {

  faFire = faFire;
  faWalking = faWalking;
  faHollyBerry = faHollyBerry


  constructor() { }

  ngOnInit() {
  }

}

import { Component, OnInit } from '@angular/core';
import {RegistrationService} from "../registration-page/registration.service";
import {Place, YandexSearchService} from "./yandex-search.service";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {SavePlacesService} from "./save-places.service";
import {UserService} from "../user.service";

@Component({
  selector: 'app-yandex-map',
  templateUrl: './yandex-map.component.html',
  styleUrls: ['./yandex-map.component.css']
})
export class YandexMapComponent implements OnInit {

  public map :any;
  public searchYandex: any;
  public geoPosition: Array<any>; //Geoposiotion of user( city)

  public results: any;

  public searchText: string;

  public lengthResponse: number;
  public places: any; // places. returning as a result of search
  public myCollection: any // Collection of current interests to search

  public placeToGo: any; //Var to save place after choosing

  //new files 26.04.2019
  public idOfMeeting$: bigint;
  public toSearch$: Array<any> = new Array<any>(); // CHECK 05.05.2019
  //new files 30.04.2019
  public isShowMoreInfo: Array<boolean> = new Array<boolean>();
  //new files 01.05.2019
  public elementOfSearching: any;
  public isPlacesLoaded: boolean;
  public arrayOfChosenPlaces: Array<boolean> = new Array<boolean>();
  //new files 02.05.19
  public urlForAvatars: Array<string> = new Array<string>();
  public favicons: Array<string> = new Array<string>();
  public arrayOfAcceptedPlaces: Array<Place> = new Array<Place>();
  //new files 05.05.2019
  public getInterestsOrCategories$: Array<any> = new Array<any>();
  public placeHours: Array<string> = new Array<string>();
  public placePictures: Array<string> = new Array<string>();


  constructor(private yaService: YandexSearchService,
              private actRoute: ActivatedRoute,
              private http: HttpClient,
              private savePlacesService: SavePlacesService,
              private userService: UserService,
              private router: Router) {
  }

  ngOnInit() {
    this.actRoute.queryParams
      .subscribe(params =>{
        console.log(params['id']);
        this.idOfMeeting$ = params['id'];

        console.log(params['toSearch']);
        this.getInterestsOrCategories$ = JSON.parse(params['toSearch']);
        console.log("After parsing " + this.getInterestsOrCategories$);

        //NewShit 01.05.2019
        if(this.getInterestsOrCategories$[0].name != undefined){
          for(let elem in this.getInterestsOrCategories$){
            console.log("No undefined");
            this.toSearch$.push(this.getInterestsOrCategories$[elem].name);
          }
        }
        else{
          this.toSearch$ = this.getInterestsOrCategories$;
        }

        if(this.toSearch$.length > 1){
          let varString = '';
          for (let el in this.toSearch$) {
            varString += this.toSearch$[el] + ' ';
          }
          console.log("THIS IS FOR SEARCHING DEFAULT" + varString);
          this.elementOfSearching = varString;
        }
        else{
          this.elementOfSearching = this.toSearch$[0];
        }

        //ReDefine search text
        this.searchYandex = this.toSearch$;
        console.log(this.searchYandex);

        ymaps.ready().then(() => {
          ymaps.geolocation.get().then(function (result) {
            localStorage.setItem('geoPos', JSON.stringify(result.geoObjects.get(0).geometry.getCoordinates()));
          });

          this.geoPosition = JSON.parse(localStorage.getItem('geoPos'));

          console.log("Get Coordinates");
          console.log(this.geoPosition[0] + ' ' + this.geoPosition[1]);

          this.map = new ymaps.Map('map', {
            center: [this.geoPosition[0], this.geoPosition[1]],
            zoom: 11.5,
            controls: []
          });

          this.searchYandex = new ymaps.control.SearchControl({
            options: {
              provider: 'yandex#search',
              noPopup: true,
              noSuggestPanel: true,
              //Опция отключает автоматический выбор результата поиска.
              noSelect: true
            }
          });

          this.myCollection = new ymaps.GeoObjectCollection({},{
            preset: 'islands#redIcon'
          });

          console.log('myColl');
          console.log(this.myCollection);

          this.map.geoObjects.add(this.myCollection);

          this.doSearchByRest(this.elementOfSearching);

        });

      });

    (async () => {
      // Do something before delay
      console.log('before delay');

      await this.delay(4000);

      // Do something after
      console.log('after delay');

      //CLICK REALIZATION- INITIALIZATION
    })();



  }

  //Parametrized 01.05.2019
  doSearchByRest(searchingElement: any){

     this.myCollection.removeAll();
     console.log(searchingElement); //this.toSearch$

     this.yaService.yaSearch(searchingElement, this.geoPosition, this.map.getBounds())
       .subscribe(response=>{

         this.parseYaResponse(response);

         console.log('yaPlaces ');
         console.log(this.places);


         this.isPlacesLoaded = true;

         for (var i = 0; i < this.lengthResponse; i++){
           this.myCollection.add(new ymaps.Placemark(
             [
               this.places[i]['geometry']['coordinates'][1],
               this.places[i]['geometry']['coordinates'][0]
             ],
             {
               iconContent:'A'+i,
               balloonContentHeader: this.places[i]['properties']['CompanyMetaData']['name'],//this.toSearch$
               balloonContentBody: this.places[i]['properties']['description'],
               balloonContentFooter: this.places[i]['properties']['CompanyMetaData']['url'],
               // + '<br>' + 'Address: ' + this.places[i]['properties']['description']
               //   + '<br>' + 'URL: ' + this.places[i]['properties']['CompanyMetaData']['url']
             })

           );
           if(this.places[i]['properties']['CompanyMetaData']['url'] != undefined){
             this.isShowMoreInfo[i] = false;
             this.urlForAvatars[i] = this.places[i]['properties']['CompanyMetaData']['url'].replace(/(^\w+:|^)\/\//, '');
             this.favicons[i] = 'https://www.google.com/s2/favicons?domain=' + this.urlForAvatars[i];
           }
           if(this.places[i]['properties']['CompanyMetaData']['Hours']!= undefined){
              this.placeHours[i] = this.places[i]['properties']['CompanyMetaData']['Hours']['text'];
           }
         }


         //Getting mark on click
         this.myCollection.events.add('click', function (e) {
           var placemark = e.get('target'); // метка в коллекции на которую кликнули

           console.log(placemark);
           placemark.iconColor = '#f9afaf';
         });

         console.log("Получить границы");
         console.log(this.map.getBounds());

         // this.acceptPlace();
        //
     });

  }

  parseYaResponse(response: any){
    this.lengthResponse = response['features'].length;

    this.places = response['features'];
    for(let i = 0; i< this.lengthResponse; i++){
      console.log(this.places[i]['geometry']['coordinates'] + ' ' +
        this.places[i]['properties']['CompanyMetaData']['name']);
    }
  }

  acceptPlace(){
    let counter = 0;
    this.arrayOfAcceptedPlaces = [];
    for (let i = 0; i < this.arrayOfChosenPlaces.length; i++) {
      if(this.arrayOfChosenPlaces[i] == true){
        counter++;
        this.arrayOfAcceptedPlaces.push(
          new Place(this.places[i]['properties']['CompanyMetaData']['name'],
            this.places[i]['properties']['description'],
            this.places[i]['properties']['CompanyMetaData']['url'],
            this.placeHours[i])
        )
      }
    }
    if( counter == 0){
      alert("Accept at least 1 place go!");
    }
    else{
      // alert("Well done!");sdfzdf
      console.log(this.arrayOfAcceptedPlaces);
      this.savePlacesService.savePlaces(
        this.userService.currentUser().name,
        this.idOfMeeting$, this.arrayOfAcceptedPlaces).subscribe(response =>{
          console.log(response);
          this.router.navigate(['/meetings']);
      });

    }
  }

  showMoreInfo(i: number) {
    console.log("was " + this.isShowMoreInfo[i]);
    this.isShowMoreInfo[i] = !this.isShowMoreInfo[i];
    console.log("and now " + this.isShowMoreInfo[i]);
  }
  //01.05.2019

  onChangeSelect(event) {
    console.log(event.target.value);
    this.elementOfSearching = event.target.value;
    this.doSearchByRest(this.elementOfSearching);
    // console.log; asdf
  }

  async delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  checkboxChange(event, elem, index){
    console.log("Checkbox changed");
    console.log(event.target.value);
    console.log(elem);
    console.log(this.arrayOfChosenPlaces[index])
  }



}



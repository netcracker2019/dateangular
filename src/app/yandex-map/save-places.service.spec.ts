import { TestBed } from '@angular/core/testing';

import { SavePlacesService } from './save-places.service';

describe('SavePlacesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SavePlacesService = TestBed.get(SavePlacesService);
    expect(service).toBeTruthy();
  });
});

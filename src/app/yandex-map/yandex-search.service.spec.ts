import { TestBed } from '@angular/core/testing';

import { YandexSearchService } from './yandex-search.service';

describe('YandexSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: YandexSearchService = TestBed.get(YandexSearchService);
    expect(service).toBeTruthy();
  });
});

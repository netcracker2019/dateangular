import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Place} from "./yandex-search.service";

@Injectable({
  providedIn: 'root'
})
export class SavePlacesService {

  constructor(private http: HttpClient) { }


  savePlaces(personName: string, idOfMeeting: bigint, places: Array<Place>): Observable<any>{
    let body = {
      name: personName,
      idOfMeeting: idOfMeeting,
      places: places
    };
    return this.http.post('http://localhost:8080/savePlacesToVote', body, {observe: 'response'});
  }
}

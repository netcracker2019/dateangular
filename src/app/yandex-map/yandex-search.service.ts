import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {Person} from "../registration-page/registration.service";

export class Place {

  name: string;
  address: string;
  url: string;
  hours: string;

  constructor(name: string, address: string, url: string, hours: string) { //place: string, address: string
    this.name = name;
    this.address = address;
    this.url = url;
    this.hours = hours;
  }
}


@Injectable({
  providedIn: 'root'
})
export class YandexSearchService {


  constructor(private http: HttpClient) { }

  yaSearch(val: string, geoPosition: Array<number>, mapBounds: Array<Array<number>>){
  //6a5fc931-201d-499f-936f-98f6ae3e0795
    // https://search-maps.yandex.ru/v1/
    //     ? [apikey=<ключ>]
    //   & [text=<поисковый запрос>]
    //   & [type=<типы объектов>]
    //   & [lang=<язык ответа>]
    //   & [ll=<центр области поиска>]
    //   & [spn=<размеры области поиска>]
    //   & [bbox=<координаты области поиска>]
    //   & [rspn=<не искать за пределами области поиска>]
    //   & [results=<количество результатов в ответе>]
    //   & [skip=<количество пропускаемых результатов>]
    //   & [callback=<имя функции>]

    //JSON.parse
    console.log('YaService arg ' + val);

    //+
    //       '&ll=50.450100,30.523400spn=0.01,0.01'
    console.log('YandexService');
    console.log(geoPosition[0].toString() + ' ' + geoPosition[1].toString());
    console.log('   *  ');
    console.log( '&bbox=' + mapBounds[0][1] + ',' + mapBounds[0][0]
      + '~' + mapBounds[1][1] + ',' + mapBounds[1][0]);

     return (this.http.get('https://search-maps.yandex.ru/v1/' +
      '?apikey=6a5fc931-201d-499f-936f-98f6ae3e0795' +
      '&text=' + val +
      '&lang=en_RU'+
      '&results=7' +
        '&ll=' + geoPosition[1].toString() + ',' + geoPosition[0].toString()
         // +'&spn=0.1,0.1'
         + '&bbox=' + mapBounds[1][1] + ',' + mapBounds[1][0]
         + '~' + mapBounds[0][1] + ',' + mapBounds[0][0]
        //  + '&rspn=1'
        )
    );
  }
}

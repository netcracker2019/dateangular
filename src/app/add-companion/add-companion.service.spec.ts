import { TestBed } from '@angular/core/testing';

import { AddCompanionService } from './add-companion.service';

describe('AddCompanionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddCompanionService = TestBed.get(AddCompanionService);
    expect(service).toBeTruthy();
  });
});

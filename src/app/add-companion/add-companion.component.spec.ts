import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCompanionComponent } from './add-companion.component';

describe('AddCompanionComponent', () => {
  let component: AddCompanionComponent;
  let fixture: ComponentFixture<AddCompanionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCompanionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCompanionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

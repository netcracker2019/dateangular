import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Person} from "../registration-page/registration.service";

@Injectable({
  providedIn: 'root'
})
export class AddCompanionService {

  constructor(private http: HttpClient) { }


  addCompanion(email: string ): Observable<any>{
    return this.http.post('http://localhost:8080/addCompanion', email, {observe: 'response'});
  }

  acceptAndCreateMeeting(nameOfCreator: string, companions: Array<string>): Observable<any>{
    return this.http.post('http://localhost:8080/createMeeting?name=' + nameOfCreator, companions, {observe: 'response'});
  }

}

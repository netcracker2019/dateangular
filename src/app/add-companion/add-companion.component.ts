import { Component, OnInit } from '@angular/core';
import {Person} from "../registration-page/registration.service";
import {AddCompanionService} from "./add-companion.service";
import {Router} from "@angular/router";
import {UserService} from "../user.service";
import {faMinus, faPlus, faUserFriends} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-add-companion',
  templateUrl: './add-companion.component.html',
  styleUrls: ['./add-companion.component.css']
})
export class AddCompanionComponent implements OnInit {


  companions: Array<string> = new Array<string>(); //Person
  isRegistered: Array<string> = new Array<string>(); // isRegistered or not

  isAdding: boolean;
  searchEmail: string; // Searching field (by email)

  searchError: string;
  lengthError: string = "";
  lastResponse: string;

  //ICONS
  faPlus = faPlus; // icon pf plus
  faMinus = faMinus; // icon pf minus
  faUserFriends = faUserFriends;

  //17.05.2019
  progressAdd: boolean;


  constructor(private companionService: AddCompanionService,
              private userService: UserService,
              private router: Router) { }

  ngOnInit() {
    // this.refreshCompanions();

  }

  refreshCompanions(){
    // this.companionService.
  }

  setIsAdding(){
    this.isAdding = !this.isAdding;
  }

  addNewCompanion(){


    this.companionService.addCompanion(this.searchEmail).subscribe(
      resp=>{
        // console.log(resp);
        this.lastResponse = resp.body;
        console.log(resp.body);
        if (this.lastResponse === 'NOT_FOUND') {
          this.searchError = 'THERE IS NO LOGIN LIKE THIS';
          console.log(this.searchError);
          if(this.companions.includes(this.searchEmail)){
            alert("You have already added this email");
          }
          else{
            this.companions.push(this.searchEmail);
            this.isRegistered.push("NOT REGISTERED");
          }
          return;
        }
        else if (this.lastResponse === 'FOUND') {
          this.searchError = "";
          if(this.companions.includes(this.searchEmail)){
            alert("You have already added this email");
          }
          else{
            this.companions.push(this.searchEmail); //NEED TESTING
            this.isRegistered.push('');
          }
        }
      }
    );
    this.setIsAdding(); //NEED TESTING!
  }

  deleteCompanion(index: number){
    this.companions.splice(index,1);
    this.isRegistered.splice(index,1);
  }

  goToMeetings(){
    if (this.companions.length == 0){
      // this.lengthError = "No companions!";
      alert('Please, add at least 1 companion');
      console.log(this.lengthError);
    }
    else
    {
      this.progressAdd = !this.progressAdd;

      this.lengthError = "";
      this.companionService.acceptAndCreateMeeting(this.userService.currentUser().name, this.companions)
        .subscribe(resp =>{
          console.log(resp.body);
          this.router.navigate(['/meetings']);
        });
    }
  }

  logOut() {
    this.userService.logOut();
    this.router.navigate(['/']); //To main page
  }

  goToProfile(){
    this.router.navigate(['/profile']);
  }

}

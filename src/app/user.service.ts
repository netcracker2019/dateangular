import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Person} from './registration-page/registration.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  login(login: string) {
    localStorage.setItem('currentUser', JSON.stringify({token: Math.random(), name: login}));
  }
  currentUser() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }
  logOut() {
    return localStorage.removeItem('currentUser');
  }
  isAdmin(login: string) {
    return this.http.post('http://localhost:8080/isAdmin', login, {observe: 'response'});
  }
  getAllUsers(): Observable<any> {
    return this.http.get('http://localhost:8080/allUsers', {observe: 'response'});
  }
  editUser(p: Person) {
     return this.http.post('http://localhost:8080/editPerson', p);
  }
  deleteUser(p: Person) {
    return this.http.post('http://localhost:8080/deletePerson', p);
  }
}

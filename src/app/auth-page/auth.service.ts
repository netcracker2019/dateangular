import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Person} from '../registration-page/registration.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }
  auth(person: Person) {
    return this.http.post('http://localhost:8080/auth', person, {observe: 'response'});
  }
}

import { Component, OnInit } from '@angular/core';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';
import {Person} from '../registration-page/registration.service';
import {UserService} from '../user.service';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.css']
})
export class AuthPageComponent implements OnInit {

  userLogin: '';
  userPassword: '';
  error: string;
  lastResponse: string;

  hide = true;

  constructor(private authPageService: AuthService, private router: Router, private userService: UserService) { }

  ngOnInit() {
  }

  auth() {
    if (this.userLogin === '') {
      this.lastResponse = 'Login field must not be empty';
      return;
    }
    if (this.userPassword === '') {
      this.lastResponse = 'Password field must not be empty';
      return;
    }
    this.authPageService.auth(new Person(this.userLogin, this.userPassword)).subscribe( response => {
      this.lastResponse = response.body.toString();
      if (this.lastResponse === 'NOT_FOUND') {
        this.error = 'User with this login does not exist';
      }
      if (this.lastResponse === 'EXPECTATION_FAILED') {
        this.error = 'Login and/or password are/is wrong';
      }
      if (this.lastResponse === 'FOUND') {
        this.error = 'Authorization has been successful';
        this.userService.login(this.userLogin);
        this.router.navigate(['/meetings']); // must be profile
      }
      console.log(this.lastResponse);
      this.lastResponse = '';
      this.userLogin = '';
      this.userPassword = '';
    });
  }

  goToMain(){
    this.router.navigate(['/']);
  }

  goToRegister(){
    this.router.navigate(['/registration'])
  }

}

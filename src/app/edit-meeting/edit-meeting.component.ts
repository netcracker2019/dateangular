import { Component, OnInit } from '@angular/core';
import {ParamMap, Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute} from "@angular/router";
import {UserService} from "../user.service";
import {faMinus, faPlus} from '@fortawesome/free-solid-svg-icons';
import {SaveUserInterestsService} from "./save-user-interests.service";
import {Interest} from "../meetings/meetings.service";
import {FormGroup, ReactiveFormsModule, FormsModule, FormControl} from "@angular/forms";

@Component({
  selector: 'app-edit-meeting',
  templateUrl: './edit-meeting.component.html',
  styleUrls: ['./edit-meeting.component.css']
})
export class EditMeetingComponent implements OnInit {

  idOfMeeting$: bigint;

  faPlus = faPlus; // icon pf plus
  faMinus = faMinus; // icon pf minus

  userInterests: Array<any> = new Array<any>();//Map

  interestsFromDB: Array<Interest>;
  myForm: FormGroup;
  myInterests: Array<Interest> = new Array<Interest>();
  currentHobby: Interest = null;


  constructor(private router: Router,
              private http: HttpClient,
              private actRoute: ActivatedRoute,
              private userService: UserService,
              private saveInterestsService: SaveUserInterestsService) { }

  ngOnInit() {
    this.actRoute.queryParams
      .subscribe(params =>{
        console.log(params['id']);
        this.idOfMeeting$ = params['id'];
      });

    //Adding Form
    this.myForm = new FormGroup( {
      FirstForm : new FormControl('')
      }
    );

    this.refreshInterests();
  }

  onChangeSelect(event){
    console.log(event.target.value);
    console.log(this.interestsFromDB[event.target.value-1]);

    this.currentHobby = this.interestsFromDB[event.target.value-1];
    console.log(this.currentHobby);

  }

  logOut() {
    this.userService.logOut();
    this.router.navigate(['/']); //To main page
  }

  goToProfile(){
    this.router.navigate(['/profile']);
  }

  addNewHobby(){
    if(this.currentHobby == null){
      return;
    }
    else{
      if(this.myInterests.includes(this.currentHobby)){
        alert("You have already added this hobby");
      }
      else{

        // this.testIntrs.push([this.currentHobby,this.currentCategory]);
        this.myInterests.push(this.currentHobby);
        this.currentHobby = null;

        console.log("My interests");
        console.log(this.myInterests);
      }
    }
  }

  deleteHobby(index: number){
    this.myInterests.splice(index,1);
  }

  goToMap(){
    if(this.myInterests.length == 0){
      alert('Add at least 1 hobby');
    }
    else{
      this.userInterests.push(this.userService.currentUser().name);
      this.userInterests.push(this.idOfMeeting$);
      this.userInterests.push(this.myInterests); //CHECK
      this.saveInterestsService.saveUserInterests(this.userInterests)
        .subscribe(resp=>{
          console.log(resp.body);
          this.router.navigate(['/meetings']); //May be go to the map?
        });
      console.log('Do all add their interests?'); //NEEd continue realization
    }
  }

  refreshInterests(){
    this.saveInterestsService.getAllInterests()
      .subscribe(response =>{
        console.log(response.body);
        console.log(response);
        this.interestsFromDB = response.body;
      });
  }


}

import { TestBed } from '@angular/core/testing';

import { SaveUserInterestsService } from './save-user-interests.service';

describe('SaveUserInterestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SaveUserInterestsService = TestBed.get(SaveUserInterestsService);
    expect(service).toBeTruthy();
  });
});

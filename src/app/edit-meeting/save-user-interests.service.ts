import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SaveUserInterestsService {

  constructor(private http: HttpClient) { }

  saveUserInterests(userInterests: Array<any>): Observable<any>{
    return this.http.post('http://localhost:8080/saveInterestsFromUser', userInterests, {observe: 'response'});
  }

  getAllInterests(): Observable<any>{
    return this.http.get('http://localhost:8080/getAllInterests',{observe: 'response'} );
  }
}

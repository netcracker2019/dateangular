import { Component, OnInit } from '@angular/core';
import {UserService} from '../user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  currentUser: any;
  isAdmin: boolean;

  constructor(private userService: UserService, private routes: Router) { }

  ngOnInit() {
    this.currentUser = this.userService.currentUser().name;
    console.log(' This is current user ' + this.currentUser);
    this.userService.isAdmin(this.userService.currentUser().name).subscribe(data => {
       this.isAdmin = data.body.toString() === 'true';
    });
  }
  logOut() {
    this.userService.logOut();
    this.routes.navigate(['/']); //To main page
  }

  goToAdminPanel() {
    this.routes.navigate(['/admin-manage']);
  }
}

import { Component, OnInit } from '@angular/core';
import {UserService} from '../user.service';
import {Router} from '@angular/router';
import {Person, RegistrationService, Roles} from '../registration-page/registration.service';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {

  people: Array<any>;
  editableNumber = -1;
  name: string;
  email: string;
  phone: string;
  role: Roles;
  isCreatable: boolean;
  newName: string;
  newEmail: string;
  newPhone: string;
  newRole: Roles;
  newPassword: string;

  constructor(private userService: UserService, private routes: Router, private registerService: RegistrationService) { }

  ngOnInit() {
    this.refreshList();
  }

  refreshList() {
    this.userService.getAllUsers().subscribe(data => {
      console.log(data.body);
      this.people = data.body;
    });
  }
  goToMainPage() {
    this.routes.navigate(['/main']);
  }
  edit(i, p: Person) {
    this.editableNumber = i;
    this.name = p.name;
    this.phone = p.phone;
    this.email = p.email;
    this.role = p.role;
    console.log(i);
  }

  approve(p: Person) {
    this.editableNumber = -1;
    this.refreshList();
    this.userService.editUser(new Person(this.name, '', this.email, this.phone, this.role)).subscribe(response => {
      this.refreshList();
    });
  }
  delete(p: Person, i) {
    this.userService.deleteUser(new Person(p.name, '', p.email, p.phone, p.role)).subscribe(e => {
      if (this.editableNumber !== -1 && this.editableNumber > i ) {
        this.editableNumber = this.editableNumber - 1;
      }
      this.refreshList();
    });
  }
  create() {
    this.registerService.registerUser(new Person(this.newName, this.newPassword, this.newEmail, this.newPhone, this.newRole), " ").
    subscribe( response => {
      this.refreshList();
      }
    );
    this.setIsCreatable(false);
    this.newRole = null;
    this.newPhone = '';
    this.newName = '';
    this.newPassword = '';
    this.newEmail = '';
  }
  setIsCreatable(isCreatable: boolean) {
    this.isCreatable = isCreatable;
  }

  goToProfile(){
    this.routes.navigate(['/profile']);
  }
}

import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Person} from "../registration-page/registration.service";



//need to Add more fields after SpringBoor realization
export class Meeting {

  id: bigint; //may be string
  name: string;
  place: string;
  address: string;
  friends: Array<Person>;

  // constructor(name: string){
  //   this.name = name;
  // }

  constructor(name: string, id: bigint ) { //place: string, address: string
    this.name = name;
    this.id = id;
    // this.place = place;
    // this.address = address;
  }
}

export class Interest{

  id: bigint;
  name: string;
  category: string;
}

// export class backEndInterest{
//   idOfMeeting: bigint;
//   nameOfInterest: string;
// }

@Injectable({
  providedIn: 'root'
})
export class MeetingsService {

  constructor(private http: HttpClient) { }

  getMeetingsAndInterests(personName: string): Observable<any>{
    return this.http.post('http://localhost:8080/getMeetings', personName);
  }

  mergeInterests(personName: string): Observable<any>{
    return this.http.post('http://localhost:8080/mergeInterests', personName);
  }

  showPlaces(personName: string): Observable<any>{
    return this.http.post('http://localhost:8080/showPlaces', personName);
  }

  addMeeting(personName: string, meeting: Meeting){
    let body = {
      username: personName,
      meeting: meeting
    };
    return this.http.post('http://localhost:8080/addMeeting', body);
  }

  deleteMeeting(personName : string, nameMeeting: string){
    let body = {
      username: personName,
      meetingName: nameMeeting
    };
    return this.http.post('http://localhost:8080/deleteMeeting', body); // DELETE
  }

  saveVotedPlacesByPerson(votedPlaces: Array<[any, number]>, name: string, meetingId: string) :  Observable<any>{
    return this.http.post('http://localhost:8080/voteForMeeting?name='+ name + '&meetingId=' +meetingId , votedPlaces, {observe: 'response'});
  }

  finishVoting(name: string): Observable<any>{
    return this.http.post('http://localhost:8080/finishVoting', name, {observe: 'response'});
  }
}



import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {UserService} from "../user.service";
import {Interest, MeetingsService} from "./meetings.service";
import {Meeting} from "./meetings.service";
import {ActivatedRoute} from "@angular/router";
import {faEdit, faGlassCheers, faMapMarkedAlt, faStar} from "@fortawesome/free-solid-svg-icons";
import {__await} from "tslib";
import {Place} from "../yandex-map/yandex-search.service";

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}

export interface Dict {
  [index: string]: Array<string>;
}

export interface NumberDict {
  [index: string]: Array<number>;
}

@Component({
  selector: 'app-meetings',
  templateUrl: './meetings.component.html',
  styleUrls: ['./meetings.component.css']
})
export class MeetingsComponent implements OnInit {

  meetings: Array<Meeting>;
  friends: any; //NEED REALIZATION

  myName: string;
  faEdit = faEdit;
  faGlassCheers = faGlassCheers;
  faMapMarkedAlt = faMapMarkedAlt;

  responseObjects: Array<any> = new Array<any>();
  objectsFromMerging: Array<any> = new Array<any>();

  ownInterests: Array<any> = new Array<any>();
  friendsInterests: Array<any> = new Array<any>();

  //03.05.19
  placesToVote: Array<Array<Place>> = new Array<Array<Place>>();
  tiles: Tile[] = [
    {text: 'One', cols: 1, rows: 1, color: 'lightblue'},
    {text: 'Two', cols: 1, rows: 2, color: 'lightgreen'},
    {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
    {text: 'Four', cols: 1, rows: 1, color: '#DDBDF1'},
  ];
  faStar = faStar;
  faviconsFromGoogle: Dict = new class implements Dict {
    [index: string]: Array<string>;
  };
  votesForMeetings: NumberDict = new class implements NumberDict {
    [index: string]: Array<number>;
  }
  //04.05.2019
  finishVotingPlace: any;
  //07.05.19
  faviconsFinish: Dict = new class implements Dict {
    [index: string]: Array<string>;
  };


  constructor(private router: Router,
              private userService: UserService,
              private meetingsService: MeetingsService,
              private actRoute: ActivatedRoute) { }

  ngOnInit() {
    this.refreshMeetingsAndInterests();
    this.refreshMergingInterests();
    this.refreshPlacesToVote();
    this.refreshFinishOfVoting();

    this.myName = this.userService.currentUser().name;


    (async () => {
      // Do something before delay
      console.log('before delay');

      await this.delay(300);

      // Do something after
      console.log('after delay');

    })();

  }

  refreshMeetingsAndInterests(){
    this.meetingsService.getMeetingsAndInterests(this.userService.currentUser().name)
      .subscribe(response =>{
        console.log(response);

        this.responseObjects = response;
        this.ownInterests = this.responseObjects["ownInterests"];
        this.friendsInterests = this.responseObjects["friendsInterests"];

        console.log(this.ownInterests);
        console.log(this.friendsInterests);

      });
  }

  refreshMergingInterests(){
    this.meetingsService.mergeInterests(this.userService.currentUser().name)
      .subscribe(response =>{
        console.log("HERE MERGING OBJECTS");
        console.log(response);

        this.objectsFromMerging = response;
      });
  }

  refreshPlacesToVote(){
    this.meetingsService.showPlaces(this.userService.currentUser().name)
      .subscribe(response =>{
        console.log("HERE places to vote!");
        console.log(response);

        this.placesToVote = response;

        for(let el of Object.keys(this.placesToVote)){
          for (let str of this.placesToVote[el]){
            if(str.url != null){
              if(typeof this.faviconsFromGoogle[el] == "undefined"){
                this.faviconsFromGoogle[el] = [];
              }
              let preUrl = str.url.replace(/(^\w+:|^)\/\//, '');
              this.faviconsFromGoogle[el].push('https://www.google.com/s2/favicons?domain=' + preUrl);
            }
            if(typeof this.votesForMeetings[el] == "undefined"){
              this.votesForMeetings[el] = [];
            }
            this.votesForMeetings[el].push(1);
          }
        }
        console.log(this.faviconsFromGoogle);

      });
  }

  createMeeting(){
    this.router.navigate(['/findPeople']);
  }

  logOut() {
    this.userService.logOut();
    this.router.navigate(['/']); //To main page
  }

  goToProfile(){
    this.router.navigate(['/profile']);
  }


  goToExactMeeting(id: string){
    this.router.navigate(['/editMeeting'],
      {queryParams: {id: id}})
  }

  async delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }


  toSearch(id: bigint){
    //Common interests
    let newJson;
    if (this.objectsFromMerging[id.toString()][0].length != 0){
      console.log("Interests");
      console.log(this.objectsFromMerging[id.toString()][0]);
      newJson = JSON.stringify(this.objectsFromMerging[id.toString()][0]);
      //Common categories
    } else if (this.objectsFromMerging[id.toString()][1].length != 0){
      console.log("Categories");
      console.log(this.objectsFromMerging[id.toString()][1]);
      newJson = JSON.stringify(this.objectsFromMerging[id.toString()][1]);
    }
    else{
      alert("NO INTERSECTS");
      return;
    }
    this.router.navigate(['/yandex'],
      {queryParams: {id: id, toSearch: newJson}});
  }

  onSliderChanged(event, index, meetingId){
    console.log("Changed slider");
    console.log(event.value);
    console.log(index);
    console.log(meetingId);
    this.votesForMeetings[meetingId][index] = event.value;
  }

  voteForExactMeeting(meetingId: string){
    let counter = 0;
    let placesVoted : Array<[any, number]> = new Array<[any, number]>(); //[any, number]

    for(let i = 0; i < this.placesToVote[meetingId].length; i++){
      if(this.votesForMeetings[meetingId][i] > 1){
        placesVoted.push([this.placesToVote[meetingId][i],
          this.votesForMeetings[meetingId][i]]);
          counter++;
      }
    }

    console.log(placesVoted);
    if (counter != 0){
      this.meetingsService.saveVotedPlacesByPerson(placesVoted,
        this.myName, meetingId).subscribe(response =>{

        console.log(response);
        this.pageRefresh();
      });
    }
    else{
      alert("Vote for at least 1 place to go");
    }
    //
    //
  }

  pageRefresh(){
    window.location.reload();
  }

  refreshFinishOfVoting(){
    console.log("Request for voting union");
    this.meetingsService.finishVoting(this.userService.currentUser().name).
      subscribe(response =>{
        console.log(response.body);

        // if (response) ///8/
      ///756
      this.finishVotingPlace = response.body;
      for(let el of Object.keys(this.finishVotingPlace)) {
          if (this.finishVotingPlace[el].url != null) {
            console.log(this.finishVotingPlace[el].url);
            this.faviconsFinish[el] = [];
            let preUrl = this.finishVotingPlace[el].url.replace(/(^\w+:|^)\/\//, '');
            // console.log("el " + el);
            this.faviconsFinish[el].push('https://www.google.com/s2/favicons?domain=' + preUrl);
          }

      }
      console.log("favicons: " + this.faviconsFinish[1]);
    });
  }

  endOfMeeting(idOfMetting: string){
    alert("You were finished meeting " + idOfMetting);
  }


}

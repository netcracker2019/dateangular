import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';

import { AppComponent } from './app.component';
import { RegistrationPageComponent } from './registration-page/registration-page.component';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';
import {PasswordModule} from 'primeng/password';
import {RegistrationService} from './registration-page/registration.service';
import { AuthPageComponent } from './auth-page/auth-page.component';
import {AuthService} from './auth-page/auth.service';
import {UserService} from './user.service';
import { MainPageComponent } from './main-page/main-page.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { MainWwwComponent } from './main-www/main-www.component';
import { MeetingsComponent } from './meetings/meetings.component';
import { AddCompanionComponent } from './add-companion/add-companion.component';
import {MeetingsService} from "./meetings/meetings.service";
import {AddCompanionService} from "./add-companion/add-companion.service";
import { EditMeetingComponent } from './edit-meeting/edit-meeting.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {SaveUserInterestsService} from "./edit-meeting/save-user-interests.service";
import { YandexMapComponent } from './yandex-map/yandex-map.component';
import {YandexSearchService} from "./yandex-map/yandex-search.service";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCheckboxModule, MatButtonModule,
  MatToolbarModule, MatCardModule, MatSelectModule, MatFormFieldModule,
  MatOptionModule} from "@angular/material";
import {MatDividerModule} from "@angular/material";
import { NewToolbarComponent } from './new-toolbar/new-toolbar.component';
import {MatIconModule, MatInputModule, MatProgressBarModule,
MatExpansionModule, MatGridListModule, MatSliderModule} from "@angular/material";


const routes = [
  {path: 'registration', component: RegistrationPageComponent},
  {path: 'auth', component: AuthPageComponent},
  {path: 'profile', component: MainPageComponent},
  {path: 'admin-manage', component: AdminPageComponent},
  {path: '', component: MainWwwComponent},
  {path: 'meetings', component: MeetingsComponent },
  {path: 'findPeople', component: AddCompanionComponent},
  {path: 'editMeeting', component: EditMeetingComponent}, // Check it
  {path: 'yandex', component: YandexMapComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    RegistrationPageComponent,
    AuthPageComponent,
    MainPageComponent,
    AdminPageComponent,
    MainWwwComponent,
    MeetingsComponent,
    AddCompanionComponent,
    EditMeetingComponent,
    YandexMapComponent,
    NewToolbarComponent
  ],
  imports: [
    BrowserModule,  FormsModule, RouterModule.forRoot(routes),
    RouterModule.forRoot(routes), HttpClientModule ,
    AppRoutingModule, PasswordModule, FontAwesomeModule,
    FormsModule, ReactiveFormsModule, BrowserAnimationsModule,
    MatCheckboxModule, MatToolbarModule, MatCardModule, MatButtonModule,
    MatSelectModule, MatFormFieldModule, MatInputModule,
    MatOptionModule, MatDividerModule, MatIconModule,
    MatProgressBarModule, MatExpansionModule, MatGridListModule,
    MatSliderModule
  ],
  providers: [RegistrationService,
              AuthService,
              UserService,
              MeetingsService,
              AddCompanionService,
              SaveUserInterestsService,
              YandexSearchService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
